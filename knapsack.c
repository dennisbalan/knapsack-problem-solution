#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
int max(int,int);
int stuff(int,int,int *);
int function(int,int,int*,int*);
//this program takes an input of stdin and performs a knapsack calculation on the data. First line of data is capacity for the knapsack,other lines are name;weight;value which hold the knapsack data.After knapsack algorithm is performed, items in knapsack are found and printed to the screen, total weight and total value are also printed
int main(void) {
	//take time
	clock_t start = clock();
	//capacity is how much a knapsack can carry, initially set to 0
	int capacity = 0;
	//number_of_items is how much items are available to be put in the knapsack
	int number_of_items = 0;
	//values array holds an item's value,weights hold an item's weight, names holds an item's name as 127-bit string. an element at i =0 owns the data values[0]
	int values[128];
	int weights[128];
	char *names[128][127];
	//initialize empty string of 15 bits to read lines all over and over again
	char line[150] = "";
	//create buffer for gets, yes we will use gets to read strings, its unsafe I took 425 but I just want to get done
	char *string;
	//read the first line to get capacity
	scanf("%d",&capacity);
	//read the other lines until there are no more lines to read. while reading assign each element in the line to its correct array, for every line increase, increment the size of number_of_items
	while((string = gets(line)) != NULL){
		int k = 0;
		if(strcmp(line,"") != 0){
			char *string = strtok(line,";");
			strcpy(names[number_of_items-1],string);
			int v = 0;
			string = strtok(NULL,";");
			char *copy = string;
			weights[number_of_items-1] = atoi(copy);
			string = strtok(NULL,";");
			copy = string;
			values[number_of_items-1] = atoi(copy);
			v++;
			k++;
		}
		number_of_items++;
	}
    
	number_of_items = number_of_items-1;
	//create table m to perform knapsack on off number_of_items+1 as x dimension and capacity+1 as y dimension
	int m[number_of_items+1][capacity+1];
	//set each element to 0
	for(int i = 0; i < (number_of_items+1);i++){
		for(int j = 0;j < (capacity+1); j++){
			m[i][j] = 0;
		}
	}
	//perform knapsack algorithm iteratively. after finishing calculations you should have the total cost for the solutiom
	//i is the index of an item's position, j is an index of the item's weight
	for(int i = 0;i <= number_of_items;i++){
		for(int j = 0; j <= capacity;j++){
			//base case/safety for 0
			if(i == 0 || j == 0){
			    m[i][j] = 0;
			}
			//if weight of index i is bigger than j, switch index of i to i-1
			if(weights[i] > j){
				m[i][j] = m[i-1][j];
			}
			else{
				//choose the maximum of either m[i-1][j] or values[i] + m[i-1][j-weights[i]] and set the maximum to m[i][j]
				//m[i-1][j] just changes your i index to i-1
				//values[i] + m[i-1][j-weights[i]] basically sends you from one item to the other
				m[i][j] = max(m[i-1][j],values[i] + m[i-1][j-weights[i]]);
			}
		}
		
	}
	//answer is the total value max, the results of the previous for loop
	int answer = m[number_of_items][capacity];
	int a = number_of_items;
	int b = capacity;
	//mutable final solution var to work on
	int c = answer;
	//array of items in knapsack
	int d[number_of_items];
	//amount of items in knapsack
	int e = 0;
	//using the top-to-bottom approach, find the elements in the array m that are part of the knapsack
    for(a = number_of_items+1;a >= 0;a = a-1) {
    	//check to see if m[a][b] > m[a-1][b], if so you found the location of an item in the knapsack and save the a (number_of_items) index in the d array
    	//c > 0 is to prevent extra lines/unneeded data
        if((m[a][b] > m[a-1][b]) && c > 0){
			d[e] = a;
        	b = b-weights[a];
        	c = c-values[a];
        	//check to see if value is not 0,else break
        	if(c > 0){
        		e++;
        	}
        	else{
        		break;
        	}
        }
        
	}
	//set total_weight variable to 0, it is the total weight of the items in the knapsack
	int total_weight = 0;
	//iterate through items array d and print out the needed information, plus sum up total weight
	for(int i = e; i >= 0; i = i-1){
		if(d[i] <= number_of_items){
			printf("%s,%d,%d\n",names[d[i]],weights[d[i]],values[d[i]]);
		}
		total_weight = total_weight+weights[d[i]];
	}
	printf("total weight: %d\n",total_weight);
	printf("total value: %d\n",answer);
	double time_taken_in_seconds = (double)( clock() - start ) / CLOCKS_PER_SEC;
	double time_taken_in_microseconds = time_taken_in_seconds * 1000000.0;
	printf("time taken in microseconds:%f",time_taken_in_microseconds);
	
	
}
//function that returns the max int of 2 int
int max(int a,int b){
	if(a > b){
		return a;
	}
	if(b > a){
		return b;
	}
	if(a == b){
		return b;
	}
}